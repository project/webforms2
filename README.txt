
Web Forms 2.0
------------------------
This package of modules has no relation to the Webform module (http://drupal.org/project/webform). This is a set of modules for working with the Web Forms 2.0 specification (http://www.whatwg.org/specs/web-forms/current-work/), which is an attempt to bring advanced forms functionality to browsers in a standardized way. Unfortunately, the specification has not yet evolved into an official standard, and browsers do not yet implement it. However, an in-progress, cross-browser javascript implementation is available (http://code.google.com/p/webforms2/). These modules integrate that implementation into Drupal.

To install, download the javascript implementation from http://code.google.com/p/webforms2/ and place the files into the "webforms2/lib" folder. Upload these modules to your site's modules folder, and enable the "webforms2" module and any additional desired modules.

webforms2.module simply adds the javascript implementation. It has no other functionality.

webforms2_repetition.module adds a "webforms2_repetition" element type to Drupal's Forms API, enabling developers to take advantage of the Web Forms 2.0 repetition model in their forms.

webforms2_heterogeneous_repetition.module adds a "webforms2_heterogeneous_repetition" element type to Drupal's Forms API, enabling developers to create repetitions similar to the Web Forms 2.0 repetition model, but with multiple different templates available for each block.

