<?php

/**
 * @file
 * Integration issues for tinymce
 */

/**
 * Implementation of hook_tinymce_init_settings_alter().
 */
function webforms2_repetition_tinymce_init_settings_alter(&$paInitSettings, $sTextareaName, $sTinyMceThemeName, $bIsRunning) {
  // We don't want tinymce initializing textareas inside repetition templates
  if (strstr($sTextareaName, '-template')) {
    unset($paInitSettings['elements']);
  }
}
