/**
 * Plugin for fixing problems that occur when instances of the
 * tinymce control get moved around in the document tree
 */
Drupal.webforms2Repetition.subscribe({

/**
 * Properties
 */
instancesDisabledForBlockMovement: null,

/**
 * afterBlockAdded
 * Auto-enable instances that should start off enabled.
 */
afterBlockAdded: function(oBlock) {
	var a = this.getInstancesToAutoEnable(oBlock);
	this.toggleInstances(a);
},

/**
 * beforeBlockMoved
 * Disable all currently enabled instances.
 */
beforeBlockMoved: function(oBlock) {
  var a = this.getEnabledInstances(oBlock);
  this.toggleInstances(a);
  this.instancesDisabledForBlockMovement = a;
},

/**
 * afterBlockMoved
 * Re-enable the previously disabled instances.
 */
afterBlockMoved: function(oBlock) {
	this.toggleInstances(this.instancesDisabledForBlockMovement);
	this.instancesDisabledForBlockMovement = null;
},

/**
 * getAllInstances
 * Return an array of all TinyMCE instances within the block, enabled or disabled.
 */
getAllInstances: function(oBlock) {
  var i, a, o, result, sExpression;
  
  result = [];
	a = $('textarea', oBlock).get();
	// We want to exclude any objects that are inside of repetition templates.
	sExpression = '.webforms2_repetition_template, .webforms2_heterogeneous_repetition_template, .webforms2_heterogeneous_repetition_inner_template';
	for (i=0; i < a.length; i++) {
		o = a[i];
		if (o.id && !$(o).parents(sExpression).length && $('#wysiwyg4' + o.id.substr(5), oBlock).length) {
			result.push(o);
		}
	}	

	return (result); 
},

/**
 * getEnabledInstances
 * Return an array of all TinyMCE instances within the block that are currently enabled.
 */
getEnabledInstances: function(oBlock) {
	var i, a, o, result;
	
  result = [];
	a = this.getAllInstances(oBlock);
	for (i=0; i < a.length; i++) {
		o = a[i];
		if (tinyMCE.getEditorId(o.id) != null) {
			result.push(o);
		}
	}		

	return (result);
},

/**
 * getInstancesToAutoEnable
 * Return an array of all TinyMCE instances within the newly added block that should be auto-enabled.
 */
getInstancesToAutoEnable: function(oBlock) {
	return (this.getAllInstances(oBlock));	
},

/**
 * toggleInstances
 * Toggle (enabled/disable) the specified instances.
 */
toggleInstances: function(a) {
	var i, o;
	if (a) {
		for (i=0; i < a.length; i++) {
		  o = a[i];
		  //alert('toggling ' + o.id);
			mceToggle(o.id, 'wysiwyg4' + o.id.substr(5));
		}
	}
}

/**
 * End of plugin definition
 */

}); 
