/**
 * webforms2_repetition.js
 */

Drupal.webforms2Repetition = {
  activeButton: null,   // Used to track which "add" button was last clicked
  subscribers: [],      // Each subscriber receives afterBlockAdded, beforeBlockMoved, and afterBlockMoved notifications
  dummy: null           // Last element in object definition can't have a comma after it, so we use this to let the real variables be moved, removed, commented out, etc.
};

/**
 * Tracking which button was last clicked is difficult, because the webforms2
 * library has its own way of managing events, rather than using the jquery framework.
 * Furthermore, it explicitly does things to the "onclick" event, in a custom way.
 * The only solution I found that seems to work reliably is to have the PHP
 * code set the "onclick" attribute on the button directly in the html. 
 */
Drupal.webforms2Repetition.onButtonClicked = function(e) {
  var oTarget = e.target ? e.target : e.srcElement;
  if (oTarget && oTarget != Drupal.webforms2Repetition.activeButton) {
    Drupal.webforms2Repetition.activeButton = oTarget;
    
    // A move button being clicked is how we know to trigger a beforeBlockMoved notification
    var sButtonType = $(oTarget).attr('type').toLowerCase();
    if (sButtonType == 'move-up' || sButtonType == 'move-down') {
      var oBlock = $wf2.getRepetitionBlock(oTarget);
      if (oBlock) {
        Drupal.webforms2Repetition.notifySubscribers('beforeBlockMoved', oBlock);
      }
    }
  }
}

/**
 * Event handler called on the repetition template, when a new block is added.
 */
Drupal.webforms2Repetition.onBlockAdded = function(e) {
  var oNewBlock;
  
  // Get the block that was just added
  oNewBlock = e.element;
  
  // Change its class from template to block
  if ($(oNewBlock).hasClass('webforms2_repetition_template')) {
    $(oNewBlock).removeClass('webforms2_repetition_template');
    $(oNewBlock).addClass('webforms2_repetition_block');
  }
  
  // If the block whose "add" button triggered the addition is a sibling
  // of the new block, move the new block to be its immediate next sibling
  if (Drupal.webforms2Repetition.activeButton) {
    var oBlockThatAdded = $wf2.getRepetitionBlock(Drupal.webforms2Repetition.activeButton);
    if (oBlockThatAdded) {
      var nIndex = $(oNewBlock).prevAll().index(oBlockThatAdded);
      if (nIndex > 0) {
        oNewBlock.moveRepetitionBlock(-nIndex);
      }
    }
  }
  
  Drupal.webforms2Repetition.activeButton = null;
  Drupal.webforms2Repetition.notifySubscribers('afterBlockAdded', oNewBlock);
}

/**
 * Event handler called on the repetition template, when a block is moved.
 */
Drupal.webforms2Repetition.onBlockMoved = function(e) {
  if (Drupal.webforms2Repetition.activeButton) {
    var sButtonType = $(Drupal.webforms2Repetition.activeButton).attr('type').toLowerCase();
    if (sButtonType == 'move-up' || sButtonType == 'move-down') {
      Drupal.webforms2Repetition.activeButton = null;
      Drupal.webforms2Repetition.notifySubscribers('afterBlockMoved', e.element);
    }
  }
}

/**
 * Event handler called on the repetition template, when a block is removed.
 */
Drupal.webforms2Repetition.onBlockRemoved = function(e) {
  Drupal.webforms2Repetition.activeButton = null;
}

/**
 * Let interested objects subscribe to events
 */
Drupal.webforms2Repetition.subscribe = function(oSubscriber) {
	Drupal.webforms2Repetition.subscribers.push(oSubscriber);
}

/**
 * Notify subscribers of anything interesting that happens
 */
Drupal.webforms2Repetition.notifySubscribers = function(sEvent, oBlock) {
  //alert(sEvent + "\n" + oBlock.innerHTML);
	var i, o;
	for (i=0; i < Drupal.webforms2Repetition.subscribers.length; i++) {
		o = Drupal.webforms2Repetition.subscribers[i];
		if (o[sEvent]) {o[sEvent](oBlock);}
	}
}

/**
 * Document Ready
 */
$(function() {
  // For each repetition template
  $(".webforms2_repetition_template").each(function() {
    // Enable it to receive an event when a block is added, moved, or removed.
    // The webforms2 library has its own way of managing custom events. Using 
    // jquery's bind function doesn't work in a cross-browser way.
    $(this).attr('onadded', 'Drupal.webforms2Repetition.onBlockAdded(event);' + $(this).attr('onadded'));
    $(this).attr('onmoved', 'Drupal.webforms2Repetition.onBlockMoved(event);' + $(this).attr('onmoved'));
    $(this).attr('onremoved', 'Drupal.webforms2Repetition.onBlockRemoved(event);' + $(this).attr('onremoved'));
  });
});

