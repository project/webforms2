<?php

/**
 * @file
 * Extends the forms API with element types that enable the creation of repetition lists.
 *
 * Usage:
 *   $form['mylist'] = array(
 *     '#type' => 'webforms2_repetition',
 *     '#template' => array(
 *       'field1' => array(...),
 *       'field2' => array(...),
 *     ),
 *   );
 */
 
/**
 * Add client-side code needed when a repetition list is used in a form.
 */
function webforms2_repetition_use() {
  static $bInited = false;
  if ($bInited) {return;}
  webforms2_use();
  $sPath = drupal_get_path('module', 'webforms2_repetition');
  drupal_add_js($sPath .'/webforms2_repetition.js');
  webforms2_repetition_load_modules('js');
  drupal_add_css($sPath .'/webforms2_repetition.css');
  webforms2_repetition_load_modules('css');
  $bInited = true;
}

/**
 * Implementation of hook_menu().
 */
function webforms2_repetition_menu($bMayCache) {
  $aItems = array();
  if ($bMayCache) {
    $aItems[] = array(
      'path' => 'webforms2_repetition/test',
      'title' => t('Unit test of Web Forms 2.0 Repetition'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('webforms2_repetition_test_form'),
      'access' => user_access('access devel information'),
      'type' => MENU_CALLBACK,
    );
  }
  else {
    webforms2_repetition_load_modules('inc');
  }
  return $aItems;
}

/**
 * Implementation of hook_elements().
 *
 * Public properties of webforms2_repetition element type:
 *   #min:               minimum number of repetition blocks allowed
 *   #max:               maximum number of repetition blocks allowed
 *   #template:          repetition template (new repetition blocks get cloned from this template)
 *   #item_attributes:   array of html attributes to apply to each repetition block
 */
function webforms2_repetition_elements() {
  $aElementTypes = array();

  // webforms2_repetition: the list as a whole
  $aElementTypes['webforms2_repetition'] = array(
    '#input' => true, 
    '#process' => array('webforms2_repetition_expand' => array()),
    '#min' => 1,
    '#max' => 1000,
  );

  // webforms2_repetition_block: a repetition block
  $aElementTypes['webforms2_repetition_block'] = array(
    '#input' => true, 
  );

  // webforms2_repetition_template: the repetition template
  $aElementTypes['webforms2_repetition_template'] = array(
    '#input' => true, 
  );

  return $aElementTypes;
}

/**
 * Expand a webforms2_repetition element
 *
 * 1. Trigger initializations required when a repetition list is used in a form.
 * 2. Use data in #value to create repetition blocks and initialize their elements' values.
 * 3. Initialize the repetition template with needed data.
 */
function webforms2_repetition_expand($aElement) {
  // A form with a repetition list is being built -- trigger relevant initialization code.
  webforms2_repetition_use();

  // Web Forms 2.0 repetition model requires the repetition template to have a unique id
  $aElement['#tree'] = true;
  $aElement['#_template_id'] = form_clean_id(join('-', $aElement['#parents']) . '-template');

  // Re-index #value and the data in $_POST
  if (is_array($aElement['#value'])) {
    $aElement['#value'] = array_merge($aElement['#value']);
  }
  if (isset($_POST)) {
    webforms2_repetition_reindex_posted_data($aElement['#parents']);
  }

  // Make sure #value has an entry for required instances
  for ($i=0; $i < $aElement['#min']; $i++) {
    if (!isset($aElement['#value'][$i])) {
      $aElement['#value'][$i] = array();
    }
  }

  // Create repetition blocks, by cloning the template, for each item that exists in #value
  if (is_array($aElement['#value'])) {
    foreach ($aElement['#value'] as $i => $v) {
      if (is_numeric($i)) {
        if (!$aElement[$i]) {
          $aElement[$i] = $aElement['#template'];
        }
        $aElement[$i] += array(
          '#type' => 'webforms2_repetition_block',
          '#value' => $v,
          '#_index' => $i,
          '#_template_id' => $aElement['#_template_id'],
          '#attributes' => $aElement['#item_attributes'],
        );
        webforms2_repetition_propogate_value($aElement[$i]);
      }
    } 
  }

  // Add properties to the repetition template
  $aElement['#template'] += array(
    '#type' => 'webforms2_repetition_template',
    '#_min' => $aElement['#min'],
    '#_max' => $aElement['#max'],
    '#_template_id' => $aElement['#_template_id'],
    '#attributes' => $aElement['#item_attributes'],
  );
  
  // Add the repetition template as a child element, keyed on [template_id], so
  // that form elements within the template follow Web Forms 2.0 spec
  $aElement['[' . $aElement['#_template_id'] . ']'] = $aElement['#template'];

  return $aElement;
}

/**
 * Themes the repetition list
 */
function theme_webforms2_repetition($aElement) {
  // I don't know if the Web Forms 2.0 spec or implementation requires a
  // container div to separate the repetition blocks and template from html
  // elements unrelated to the repetition list. Just in case, let's add it.
  $sOutput = '<div>' . $aElement['#children'] . '</div>';
  return theme('form_element', $aElement, $sOutput);
}
  
/**
 * Themes a repetition block
 */
function theme_webforms2_repetition_block($aElement) {
  // Append the repetition buttons (add, remove, up, down) to the block contents
  $aElement['#children'] .= theme('webforms2_repetition_buttons', $aElement);

  // Render the block as a fieldset
  unset($aElement['#value']);
  $sOutput = theme('fieldset', $aElement);

  // Surround the block with a div that identifies its type and index
  $aAttributes = array(
    'class' => $aElement['#type'],
    'repeat' => $aElement['#_index'],
  );
  $sOutput = '<div' . drupal_attributes($aAttributes) . '>' . $sOutput . '</div>';

  return $sOutput;
}
  
/**
 * Themes a repetition template
 */
function theme_webforms2_repetition_template($aElement) {
  // Append the repetition buttons (add, remove, up, down) to the block contents
  $aElement['#children'] .= theme('webforms2_repetition_buttons', $aElement);

  // Render the template as a fieldset
  unset($aElement['#value']);
  $sOutput = theme('fieldset', $aElement);

  // Surround the template with a div that identifies itself as the template
  $aAttributes = array(
    'id' => $aElement['#_template_id'],
    'class' => $aElement['#type'],
    'repeat' => 'template',
    'repeat-min' => $aElement['#_min'],
    'repeat-max' => $aElement['#_max'],
    'repeat-start' => 0,
  );
  $sOutput = '<div' . drupal_attributes($aAttributes) . '>' . $sOutput . '</div>';

  return $sOutput;
}
  
/**
 * Themes the repetition buttons (add, remove, up, down)
 */
function theme_webforms2_repetition_buttons($aElement) {
  $vExclude = $aElement['#_buttons_exclude'];
  if ($vExclude === 'all') {return;}
  $aExclude = is_array($vExclude) ? $vExclude : array();
  
  $sTemplateId = $aElement['#_template_id'];
  $aAttributes = $aElement['#_buttons_attributes'];
  $aAttributes['class'] = 'webforms2_repetition_buttons ' . $aAttributes['class'];

  $sOutput = '<div' . drupal_attributes($aAttributes) . '>';
  $sOnClick = 'onclick="Drupal.webforms2Repetition.onButtonClicked(event);"';
  if (!in_array('add', $aExclude))       {$sOutput .= "<button type=\"add\" template=\"$sTemplateId\" $sOnClick>&nbsp;Add&nbsp;</button>&nbsp;";}
  if (!in_array('remove', $aExclude))    {$sOutput .= "<button type=\"remove\" $sOnClick>Remove</button>&nbsp;";}
  if (!in_array('move-up', $aExclude))   {$sOutput .= "<button type=\"move-up\" $sOnClick>Move Up</button>&nbsp;";}
  if (!in_array('move-down', $aExclude)) {$sOutput .= "<button type=\"move-down\" $sOnClick>Move Down</button>";}
  $sOutput .= '</div>';

  return $sOutput;
}

/** 
 * Load all our module 'on behalfs'.
 */
function webforms2_repetition_load_modules($sType = 'inc') {
  $sPath = drupal_get_path('module', 'webforms2_repetition') . '/modules';
  $aFiles = drupal_system_listing('webforms2_repetition_.*\.' . $sType . '$', $sPath, 'name', 0);
  
  foreach ($aFiles as $oFile) {
    // The filename is webforms2_repetition_MODULENAME.<type>
    $sModule = substr_replace($oFile->name, '', 0, strlen('webforms2_repetition_'));
    if (module_exists($sModule)) {
      switch ($sType) {
        case 'inc': 
          require_once('./' . $oFile->filename); 
        break;
        case 'js': 
          drupal_add_js($oFile->filename);
        break;
        case 'css': 
          drupal_add_css($oFile->filename); 
        break;
      }
    }
  }
}

/*
 * re-index $_POST
 *
 * We want to order the list based on how it appeared to the
 * user when he submitted it. However, the webforms 2.0 specification
 * calls for the repetition blocks to not be re-indexed when they are moved,
 * so since we do not re-index client-side, we must do so server-side. 
 *
 * Warnings:
 *  1. Be careful here, php references are not pointers; 
 *     understand how they work before modifying this code.
 *  2. Modifying a superglobal like $_POST should not be done lightly;
 *     I believe it's justified in this case, given what we're trying to
 *     accomplish. Still, be on the lookout for unintended side-effects.
 */ 
function webforms2_repetition_reindex_posted_data($aParents) {
  if (isset($_POST) && is_array($aParents) && !empty($aParents) && isset($_POST[$aParents[0]])) {
    $aEdit =& $_POST;
    foreach ($aParents as $sParent) {
      if (isset($aEdit[$sParent])) {
        $aEdit =& $aEdit[$sParent];
      }
      else {
        unset($aEdit); 
        break;
      }
    }
    if (is_array($aEdit)) {
      $aEdit = array_merge($aEdit);
    }
    unset($aEdit);
  }
}

/**
 * If the element has children and #value set to an array, then propogate the
 * data in #value to the children.
 */
function webforms2_repetition_propogate_value(&$aElement) {
  if (is_array($aElement['#value'])) {
    foreach (element_children($aElement) as $sKey) {
      // do not propogate to a child if the child already has its own value
      if (isset($aElement['#value'][$sKey]) && !isset($aElement[$sKey]['#value'])) {
        $aElement[$sKey]['#value'] = $aElement['#value'][$sKey];
        webforms2_repetition_propogate_value($aElement[$sKey]);
        // after recursing, remove the #value attribute from certain types
        if (in_array($aElement[$sKey]['#type'], array(NULL, 'markup', 'fieldset'), true)) {
          unset($aElement[$sKey]['#value']);
        }
      }
    }
  }
}

/**
 * If the client-side Web Forms 2.0 implementation works correctly, then 
 * form elements within repetition blocks get submitted, but form elements
 * within repetition templates do not get submitted. However, if a client-side
 * error occurs, form elements within the repetition templates may get submitted.
 * This function can be called to correct for that.
 */
function webforms2_repetition_clean_submitted_data(&$aData) {
  if (is_array($aData)) {
    foreach (array_keys($aData) as $sKey) {
      if (strstr($sKey, '-template]')) {
        unset($aData[$sKey]);
      }
      else {
        webforms2_repetition_clean_submitted_data($aData[$sKey]);
      }
    }
  }
}

/**
 * Form for the test page
 */
function webforms2_repetition_test_form() {
  $aForm = array(
    '#redirect' => FALSE,
    'planets' => array(
      '#type' => 'webforms2_repetition',
      '#template' => array(
        'planet' => array(
          '#type' => 'textfield',
          '#title' => t('Planet'),
          '#default_value' => '',
        ),
        'moons' => array(
          '#type' => 'webforms2_repetition',
          '#template' => array(
            'moon' => array(
              '#type' => 'textfield',
              '#title' => t('Moon'),
              '#default_value' => '',
            ),
          ),
        ),
      ),
    ),
    'submit' => array(
      '#type' => 'submit',
      '#default_value' => 'Submit',
    ),
  );

  return $aForm;
}
