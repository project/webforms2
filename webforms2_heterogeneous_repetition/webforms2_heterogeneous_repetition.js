/**
 * webforms2_heterogeneous_repetition.js
 */

Drupal.webforms2Repetition.heterogeneous = {
};

/**
 * Event handler called on the repetition template, when a new block is added.
 */
Drupal.webforms2Repetition.heterogeneous.onBlockAdded = function(e) {
  var oNewBlock, oPreviousBlock, sRepetitionTemplateId;
  var sInnerTemplateSelectorId, sInnerTemplateKey, sInnerRepetitionTemplateId;
  
  // Get the block that was just added
  oNewBlock = e.element;
  
  // Change its class from template to block
  if ($(oNewBlock).hasClass('webforms2_heterogeneous_repetition_template')) {
    $(oNewBlock).removeClass('webforms2_heterogeneous_repetition_template');
    $(oNewBlock).addClass('webforms2_heterogeneous_repetition_block');
  }
  
  // Do everything homogeneous repetitions do, except trigger notifications
  Drupal.webforms2Repetition.onBlockAdded(e);
  
  // Determine which inner template is desired
  sRepetitionTemplateId = oNewBlock.repetitionTemplate.id;
  sInnerTemplateSelectorId = 'select_' + sRepetitionTemplateId;
  oPreviousBlock = oNewBlock.previousSibling;
  if (oPreviousBlock) {
    sInnerTemplateSelectorId += '-' + oPreviousBlock.repetitionIndex;
  }
  sInnerTemplateKey = $('#' + sInnerTemplateSelectorId).val();
  
  // Add the corresponding inner template block
  sInnerRepetitionTemplateId = sRepetitionTemplateId.substr(0, sRepetitionTemplateId.lastIndexOf('-')) + '-' + oNewBlock.repetitionIndex + '-' + sInnerTemplateKey;
  $('#' + sInnerRepetitionTemplateId)[0].addRepetitionBlock();
  
  // Hide the bottom adder
  Drupal.webforms2Repetition.heterogeneous.setBottomAdderVisibility(sRepetitionTemplateId, false);
}

/**
 * Event handler called on the repetition template, when a block is moved.
 */
Drupal.webforms2Repetition.heterogeneous.onBlockMoved = function(e) {
  // Do everything homogeneous repetitions do
  Drupal.webforms2Repetition.onBlockMoved(e);
}

/**
 * Event handler called on the repetition template, when a block is removed.
 */
Drupal.webforms2Repetition.heterogeneous.onBlockRemoved = function(e) {
  var oRepetitionTemplate = e.element.repetitionTemplate;
  // If the last block is removed, show the bottom adder
  if (oRepetitionTemplate && !oRepetitionTemplate.repetitionBlocks.length) {
    Drupal.webforms2Repetition.heterogeneous.setBottomAdderVisibility(oRepetitionTemplate.id, true);
  }
  // Do everything homogeneous repetitions do
  Drupal.webforms2Repetition.onBlockRemoved(e);
}

/**
 * Event handler called on the inner repetition template, when a block is instantiated.
 */
Drupal.webforms2Repetition.heterogeneous.onInnerBlockAdded = function(e) {
  // Change its class from template to block
  var oNewBlock = e.element;
  if ($(oNewBlock).hasClass('webforms2_heterogeneous_repetition_inner_template')) {
    $(oNewBlock).removeClass('webforms2_heterogeneous_repetition_inner_template');
    $(oNewBlock).addClass('webforms2_heterogeneous_repetition_inner_block');
  }
  Drupal.webforms2Repetition.notifySubscribers('afterBlockAdded', oNewBlock);
}

/**
 * Show or hide the adder that isn't inside of a repetition block
 */
Drupal.webforms2Repetition.heterogeneous.setBottomAdderVisibility = function(sRepetitionTemplateId, bVisible) {
  $('#webforms2_heterogeneous_repetition_itemAdder_' + sRepetitionTemplateId).css('display', bVisible ? '' : 'none');
}

/**
 * Document Ready
 */
$(function() {
  // For each heterogeneous repetition template
  $(".webforms2_heterogeneous_repetition_template").each(function() {
    // Enable it to receive an event when a block is added, moved, or removed.
    // The webforms2 library has its own way of managing custom events. Using 
    // jquery's bind function doesn't work in a cross-browser way.
    $(this).attr('onadded', 'Drupal.webforms2Repetition.heterogeneous.onBlockAdded(event);' + $(this).attr('onadded'));
    $(this).attr('onmoved', 'Drupal.webforms2Repetition.heterogeneous.onBlockMoved(event);' + $(this).attr('onmoved'));
    $(this).attr('onremoved', 'Drupal.webforms2Repetition.heterogeneous.onBlockRemoved(event);' + $(this).attr('onremoved'));
    
    // If the template starts off with any initial blocks, hide the bottom adder
    if (this.previousSibling) {
      Drupal.webforms2Repetition.heterogeneous.setBottomAdderVisibility(this.id, false);
    }
  });
  
  // For each heterogeneous repetition inner template
  $(".webforms2_heterogeneous_repetition_inner_template").each(function() {
    $(this).attr('onadded', 'Drupal.webforms2Repetition.heterogeneous.onInnerBlockAdded(event);' + $(this).attr('onadded'));
  });
  
});
